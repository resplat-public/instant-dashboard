# Instant Dashboard

Git repo for the instant dashboard project

## Environment

To create a virtualenv for the project, using [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/index.html)
and assuming you want to call the virtualenv 'instant-dashboard' and use a 
python 3 interpreter, do the following:

```bash
mkvirtualenv instant-dashboard --python=python3
# then in the new activated environment install the requirements:
pip install -r requirements.txt
``` 

To invoke the project virtualenv:

```bin
workon instant-dashboard
```

To leave the project virtualenv:

```bin
deactivate
```