# Overview

The instant-dashboard is the user facing front end for the managed cloud 
project. As such:

A user will be able to log in, using their University Credentials via 
an LDAP authentication system that fronts the university Active Directory
system.

All authenticated users will be able to launch and use both a Windows and a 
Linux VM. 

The idea is that those VMs will be accessible as remote desktop machines by the
user that launched them. This will be available in the browser for the user to
click on.

Only users that are known to the University Active Directory system will be
able to use the system.


Notes
 - the plan is to use [Application Credentials](https://support.ehelp.edu.au/support/solutions/articles/6000212274-application-credentials) 
   to get data from OpenStack.
 - but there is also  [OpenStack SDK applications](https://docs.openstack.org/openstacksdk/stein/user/config/configuration.html)
 - and [os-client-config Applications](https://docs.openstack.org/os-client-config/stein/user/configuration.html) 
 - re mote desktop will be done using [RDP](https://en.wikipedia.org/wiki/Remote_Desktop_Protocol) for 
        Windows or [FastX](https://www.starnet.com/fastx/) for the Linux machines
 
