# Active Directory authentication
 
Read: [How to add LDAP & Active Directory Authentication to Django](https://coderbook.com/@marcus/how-to-add-ldap-and-active-directory-authentication-to-django/)

Then review the [LDAP backend documentation](https://django-auth-ldap.readthedocs.io/en/latest/index.html)

Note that the installation instructions between the two articles are slightly
different.

Questions:

1. Do we need to login non LDAP users?
   A: No
1. Do we want to go down the dockerfile route to manage the system 
   dependencies? What does this give us? How do we manage the dependencies on 
   non Linux development machines?
   A: No, for the time being.
1. Do we want users to be able to log in even if the LDAP server is down?
   A: No.