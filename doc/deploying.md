# How to deploy the application on Ubuntu


## Set up the application server
 
First launch an instance with Ubuntu as the OS and that has a private IP address. 
Do this by selecting the following when launching:

* image name: NeCTAR Ubuntu 18.04 LTS (Bionic) amd64
* network: qh2-uom-internal
* keypair: managed-cloud
* security groups: http, ssh-int & default

**NB**: After the site has all the software installed, replace the http security group with the one created by the web 
front heat template (something along the lines of webfront_prod-http_only_security_group-... )
 
Once the instance is running ssh into it and execute the following commands:
 
```bash
# We are running on a private IP address, so we need to set up a proxy to access the internet
sudo echo 'http_proxy="http://wwwproxy.unimelb.edu.au:8000/"' >> /etc/environment
sudo echo 'https_proxy="http://wwwproxy.unimelb.edu.au:8000/"' >> /etc/environment
sudo export http_proxy="http://wwwproxy.unimelb.edu.au:8000/"
sudo export https_proxy="http://wwwproxy.unimelb.edu.au:8000/"
# make sure the operating system is up to date
sudo apt-get update
sudo apt-get -y upgrade

# install python, apache and the required libraries
sudo apt-get -y install python3 python3-pip apache2 libapache2-mod-wsgi-py3
sudo apt-get -y install git python3-dev python3-tk python3-venv
# following needed for the python-ldap package
sudo apt-get install libsasl2-dev libldap2-dev libssl-dev

# postgres
# This is a requirement for the psycopg2, the PostgreSQL-Django drivers
# Refer to /scripts/db_server/README.md for the production DB server
sudo apt-get install libpq-dev postgresql postgresql-contrib


# fetch the code and install the requirements
unset http_proxy
unset https_proxy

# copy the deploy key from pass store into your local keys folder so you
# can use Agent Forwarding to authenticate the git clone
pass deploy_key > deploy_key

# clone the git repo, and recursively install the submodules
git clone --recursive git@gitlab.unimelb.edu.au:resplat-cloud/instant-dashboard.git

cd instant-dashboard/

# make the virtual env for the application
python3 -m venv venv-instant-dashboard
source venv-instant-dashboard/bin/activate

set http_proxy
set https_proxy

# upgrade pip and install the requirements
pip3 install --upgrade pip
pip3 install -r requirements.txt
# replace <gitlab-secret-token> with the staff-api-token from pass in the line below
python -m pip install git+https://gitlab-ci-token:<gitlab-secret-token>@gitlab.unimelb.edu.au/resplat-cloud/staff_api.git

# set up the local settings
cd ~/instant-dashboard/researcher_workspace/
cp local_settings_template.py local_settings.py
nano local_settings.py
# don't forget to set the static root and the log file that is to be used...
# set STATIC_ROOT = "/home/ubuntu/static/" in local_settings.py...
# set LOGGING['handlers']['log_file']['filename'] = '/var/log/dashboard_app.log'
# set ALLOWED_HOSTS = ['dns_name of web front server']
# eg: ALLOWED_HOSTS = ['orion.cloud.unimelb.edu.au']

# prepare our app log file (the logfile specified in the local_settings file)
# the owner/group permissions are going to be set to allow both the ubuntu user and the apache server to write the file
sudo touch /var/log/dashboard_app.log   # make the file
sudo chown root:adm /var/log/dashboard_app.log  # give the root/adm group ownership
sudo chmod g+w  /var/log/dashboard_app.log  # and allow the group permission to write (ubuntu is part of the adm group)

# Then apply the migrations to the database
./manage.py migrate

# Create a super user to kick things off (using the university ldap attributes)
./manage.py createsuperuser --noinput --username mpaulo --email martin.paulo@unimelb.edu.au

# collect all the static files
mkdir /home/ubuntu/static
./manage.py collectstatic

# now test the server is all installed correctly:
# if you run the following with 80 you will need to be root, as ubuntu doesn't have permission to access the lower 
# port ranges
./manage.py runserver <ip_address_of_server>:8000 &
    
# fetch the page (use the FQDN instead of localhost:8000 if on server)
wget <FQDN>
fg
ctrl-c # to kill the running process

# view the index.html to see if all is good, then delete it
less index.html
rm index.html

# copy the puppet certs across to the server
scp instant_dashboard/vm_manager/pc.unimelb-instant-dashboard.cloud.edu.au.* ubuntu@your_vm_ip:/home/ubuntu/instant-dashboard/instant_dashboard/vm_manager/utils

# need to also change ownership of the database file and its parent directory to allow apache user to access it
sudo chown :www-data db.sqlite3 # add to the www-data group
chmod 664 db.sqlite3    # make group writeable
cd ..
sudo chown :www-data instant_dashboard

# Ok: no need to be in our virtualenv anymore
deactivate

# set up apache to serve the django app
cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/dashboard.conf
sed -i.bak '/serve-cgi-bin.conf/a \
Alias /static /home/ubuntu/static
        <Directory /home/ubuntu/static>
           Require all granted
        </Directory>

        <Directory "/home/ubuntu/instant-dashboard/instant_dashboard/researcher_workspace">
            <Files wsgi.py>
               Require all granted
            </Files>
        </Directory>

        WSGIDaemonProcess instant-dashboard python-path=/home/ubuntu/instant-dashboard/instant_dashboard python-home=/home/ubuntu/instant-dashboard/venv-instant-dashboard
        WSGIProcessGroup instant-dashboard
        WSGIScriptAlias / /home/ubuntu/instant-dashboard/instant_dashboard/researcher_workspace/wsgi.py
'

sudo a2dissite 000-default
sudo a2ensite dashboard

# test the apache config
sudo apachectl configtest

# restart apache
sudo systemctl reload apache2
```

## set up the queue and scheduler

install redis server

`sudo apt-get install redis-server`

create a service for the rq-worker:

`sudo pico /etc/systemd/system/rqworker.service`

put this in the file (substituting your own path to your project and virtual environment):
```
[Unit]
Description=Django-RQ Worker
After=network.target

[Service]
WorkingDirectory=/home/ubuntu/instant-dashboard/instant_dashboard
ExecStart=/home/ubuntu/instant-dashboard/venv-instant-dashboard/bin/python \
    /home/ubuntu/instant-dashboard/instant_dashboard/manage.py \
    rqworker

[Install]
WantedBy=multi-user.target
```

run\
`sudo systemctl enable rqworker`\
`sudo systemctl start rqworker`\
`sudo systemctl status rqworker`


create a service for the rq-scheduler\
`sudo pico /etc/systemd/system/rqscheduler.service`

put this in the file (substituting your own path to your project and virtual environment):
```
[Unit]
Description=RQScheduler
After=network.target

[Service]
WorkingDirectory=/home/ubuntu/instant-dashboard/instant_dashboard
ExecStart=/home/ubuntu/instant-dashboard/venv-instant-dashboard/bin/python \
    /home/ubuntu/instant-dashboard/instant_dashboard/manage.py \
    rqscheduler -i 5

[Install]
WantedBy=multi-user.target
```

run\
`sudo systemctl enable rqscheduler.service`\
`sudo systemctl start rqscheduler.service`\
`sudo systemctl status rqscheduler.service`


start the cron job to auto-downsize vms by accessing the page `<website url>/start_downsizing_cron_job`

#### Setting up webfront
Then the instructions in the [web front read me](../web_front/README.md) need to be followed.

## Review 

Not sure if we still need this section, don't know what's in it, proceed with caution

What did we find wrong when we did the above:

* Our release of django wasn't pinned - and a new version came out just before we did our deployment
* Needed to hack into django shell to get the first administrator enrolled via a token
* The log file ownership stopeed us from running manage.py unless we were the apache user. Hopefully this is corrected 
  above 
* We discovered that the test server is not picking up the admin bar title and colour from the settings file
* The original web front server is on a public IP: it needs to be moved to a private ip on a UoM only network
* We forgot to put the puppet keys on server, as they are not under the control of git...
* Also, the security groups on the application web server should be noted.
