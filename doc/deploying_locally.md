# How to deploy the application on your local machine


```bash
# install python and the required libraries
sudo apt-get -y install python3 python3-pip
sudo apt-get -y install git python3-dev python3-tk python3-venv
# following needed for the python-ldap package
sudo apt-get install libsasl2-dev libldap2-dev libssl-dev

# postgres
# This is a requirement for the psycopg2, the PostgreSQL-Django drivers
# For development, you could get away with using SQLite but you
# should have this handy anyway
# Refer to /scripts/db_server/README.md for the production DB server
sudo apt-get install libpq-dev postgresql postgresql-contrib

# fetch the code from the git repository
git clone https://gitlab.unimelb.edu.au/resplat-cloud/instant-dashboard.git

cd instant-dashboard/

# clone the additional feature repos
git clone https://gitlab.unimelb.edu.au/resplat-cloud/specialty_resources.git instant_dashboard/specialty_resources
git clone https://gitlab.unimelb.edu.au/resplat-cloud/tcat.git instant_dashboard/tcat
# each of these additional features can then be tracked as separate git repos, and any changes in those repos will be
# ignored by the instant-dashboard git repo

# make the virtual env for the application
python3 -m venv venv-instant-dashboard
source venv-instant-dashboard/bin/activate

# upgrade pip and install the requirements
pip3 install --upgrade pip
pip3 install -r requirements.txt
# replace <gitlab-secret-token> with the staff-api-token from pass in the line below
python -m pip install git+https://gitlab-ci-token:<gitlab-secret-token>@gitlab.unimelb.edu.au/resplat-cloud/staff_api.git

# set up the local settings
cd ~/instant-dashboard/instant_dashboard/
cp local_settings_template.py local_settings.py
nano local_settings.py
# don't forget to set the static root and the log file that is to be used...
# set LOGGING['handlers']['log_file']['filename'] = '/my/log/file.log'
# set ALLOWED_HOSTS = ['localhost', 'localhost ip']
# eg: ALLOWED_HOSTS = ['localhost', '127.0.0.1']

# create our app log file (the logfile specified in the local_settings file)
# If DEBUG=True, defaults to (PROJECT_ROOT/log/dashboard_app.log)
mkdir -p ../log
touch ../log/dashboard_app.log   # make the file

# Then apply the migrations to the database
./manage.py migrate

# Create a super user to kick things off (using the university ldap attributes)
./manage.py createsuperuser --noinput --username mpaulo --email martin.paulo@unimelb.edu.au

# copy the puppet certs to your 'instant-dashboard/instant_dashboard/vm_manager/utils' folder.
# Get these off a colleague
scp instant_dashboard/vm_manager/pc.unimelb-instant-dashboard.cloud.edu.au.* mpaulo@your_computer_ip:~/foo/bar/instant-dashboard/instant_dashboard/vm_manager/utils
```


###Port forwarding server
The Melbourne Uni firewall prevents us from using our dev machines as local servers that the VMs can curl back to when they have
completed the user_data_script, so we have set up a system for using a vm as a port forward to our dev machines, so that
the VMs can notify the dev server when they have completed the user_data_script.

To set up a forwarding server follow these instructions:

```bash
# Source your OpenStack credentials (this should be done using the instant-dashboard tennancy credentials as that's
where the  local_forwarding_server security group is)
source ~/your/file/path/your_openstack_credential.sh

# Create the port forwarding server (replacing KEYNAME and USERNAME, the keyname should be the managed_cloud key)
openstack server create --image "NeCTAR Ubuntu 18.04 LTS (Bionic) amd64" --key-name <KEYNAME> --security-group ssh-int --security-group local_forwarding_server --network qh2-uom-internal --flavor 0 <USERNAME>_forwarding_server

# SSH to your port forwarding server (the ip address should be of the form 172.26.128.XX)
ssh ubuntu@172.26.128.XX
```

Inside your server, allow port forwarding, and then restart the ssh demon, and exit the ssh session:
```bash
sudo pico /etc/ssh/sshd_config
```

change `#GatewayPorts no` to `GatewayPorts yes`

```bash
sudo systemctl restart ssh
exit
```

Start the port forwarding session from your dev machine. This will open up an otherwise regular ssh session ***that must
be kept open***

 for the port forwarding to continue, so just run this command in a separate window and minimise it.
```bash
ssh ubuntu@172.26.128.XX -R 8080:localhost:8000
```
***`NOTE: THIS SSH SESSION MUST BE KEPT OPEN FOR THE PORT FORWARDING TO WORK, AND MUST BE STARTED AGAIN IF IT CLOSES`***

Change the `NOTIFY_URL` constant in local settings to `"http://172.26.128.XX:8080"`
Also add this url to your `ALLOWED_HOSTS` constant, or set it to `["*"]` to allow all hosts
The VMs you create from your dev environment website should now notify your dev environment when they are ready

### set up the queue and scheduler

install redis server

`sudo apt-get install redis-server`

open two console windows, source you virtual environment in each, and run\
`python manage.py rqworker` in one, and\
`python manage.py rqscheduler -i 5` in the other\
these need to be left open for them to continue to run. Alternatively you can follow the instructions on 
"set up the queue and scheduler" in `deploying.md` to have these run permanently


##You're done!
```bash
# You're done!
# to run the server:
./manage.py runserver
```
## Review

Not sure if we still need this section, don't know what's in it, proceed with caution

What did we find wrong when we did the above:

* Our release of django wasn't pinned - and a new version came out just before we did our deployment
* Needed to hack into django shell to get the first administrator enrolled via a token
* The log file ownership stopped us from running manage.py unless we were the apache user. Hopefully this is corrected
  above
* We discovered that the test server is not picking up the admin bar title and colour from the settings file
* The original web front server is on a public IP: it needs to be moved to a private ip on a UoM only network
* We forgot to put the puppet keys on server, as they are not under the control of git...
* Also, the security groups on the application web server should be noted.
