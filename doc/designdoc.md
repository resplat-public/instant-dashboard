# Design Document

**DRAFT**

## Document Purpose & Audience

Purpose of this document is to capture the design of this software. 
This allows new staff to be on-boarded, reference key design decisions to build
upon, and for the design to be analyised/evaluated by others.

The approach is to identify key areas of design to document and then either
describe those areas within this document or link to it. The result should be
a "go-to" document for design where topics are documented, created, extended, 
linked.

Note: 
1. This document currently exists as a mark-down file in the repository but
may be moved elsewhere in the future.
2. This document exists in the instant dashboard repository but as the
overarching Researcher-Workspace project progresses, this may move the content
as well.


Intended audience: Project staff, Enterprise Architects

## Stakeholders

TODO (from Solution Outline)

Note - **Stakeholders** from project's endorse/consult/inform perspective

| Stakeholer | Role | Title | Endorse | Consult | Inform
| ------ | ------ | ------ | ------ | ------ | ------ |
| (Full name) | (Role in project) | (Job title) | | | |


## Background

TODO (from Solution Outline)
 * Dot points describing backgrond


TODO

 * Give high level context of why the system exists. (Objective of system)
 * Link/reference to other documents as necessary


## Initiative Description

TODO (from Solution Outline)

*  Where did this initiative come from?
*  Why is this initiative important?
*  Who are the key stakeholders?
*  What is the initiative's focusing question?


## Key High Level Requirements

TODO (from Solution Outline)

| Subject Area | Requirements |
| ------ | ------ |
| cell | cell |
| cell | cell | 


## Solution Context

TODO (from Solution Outline)

 * Users, external solutions, data inputs and outputs
 * External events to which the solution must respond
 * Events the solution generates that affect external entities



## Solution Summary

TODO (from Solution Outline)

 * Summary of the solution
 * Technical outline
 * Process impact outline
 * Organisation and people impact


## Solution Overview Diagram

TODO (from Solution Outline)

*"Sets the scene"*

* [ ]  Diagram showing the system as "black box" surrounded by its users and other systems it interacts with
* [ ]  Any further text describing the system context


## "As is" architecture

TODO (from Solution Outline)

 * Current state
 * Use colour scheme to note things that retire/modify/no change/external

## "To be" architecture

TODO (from Solution Outline)

 * Future state
 * Use colour scheme to note things that are new/modify/no change/external


## Component Description and Impact

TODO (from Solution Outline)

| ID | Type | Component | Description | Technology | Responsible | Impact |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| # | New/Modify/No change | name of | stuff | item list | who | low/medium/high |



## Interface Description

TODO (from Solution Outline)

| ID | Type | Purpose | Description | Source | Target | Interface and Pattern | Responsible |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| # | New/Modify|No change | short text | long text | ref to diagram | ref to diagram | item list | who |


## Security Architecture

TODO (from Solution Outline)


## Functional Overview

*Maybe cover:*

* Description of overall objective of system, theme & functionality
* Wireframes (link)
* Workflows/processes (link)

## Assumptions

TODO (from Solution Outline)


## Dependencies

TODO (from Solution Outline)


## Constraints & Implications
**1. Researcher Desktop is using infrastructure resources from Melbourne Research 
Cloud/OpenStack**

The most accessible with available capacity for the project at
intiation of project. The most important thing at initation is to develop a 
proof of concept that can be evaluated by researchers for sizing, functionality
and adaptations. The other/better backends and methods of delivery ought to be 
considered when steering the product before production.

## High Level Risks

TODO (from Solution Outline)


## Relevant Architecture Principles

TODO (from Solution Outline)



## Other Architecture

*Maybe cover:*

* Software architecture
* Infrastructure architecture (prod/test environments)


## Development environment/process

*Maybe cover/link-out:*

* Pre-requisites
* How to setup dev-environment.


## Operation and support

*Maybe cover/link-out*

* Deployment process Prod/test
* Support process
* Linux Desktop build process
* Windows Desktop build process
* (Service acccoutns, keys and other secret management)
* (Monitoring/alerts)




## Design Principles

**Maybe needed**

*Note any high level design principles for Researcher Workspace*

* 80-20 rule. It should be designed to service the common problems, and by exception the user can ask for IaaS (but need to be prepared to do more work/learning)
* UX: We shouldn't restrict users from an alternative method but we make the way we prefer it to work easy enough they don't have to think about trying something else


## Decision Log

**Maybe**


