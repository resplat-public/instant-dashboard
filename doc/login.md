# Login Requirements

* That non University of Melbourne people are not allowed to login
* That the university active directory server be used to login users
* That people who fail to login are shown an appropriate error message
* That the login system can be extended by easily adding more rules in future

## Error messages:

If either username or password is wrong:

    Please enter a correct username and password. Note that both fields may 
    be case-sensitive.
    
This is the default django error message. Thus it probably is best practice
for us to simply keep it as is.

If the user is not entitled to log in to use the system:

    You do not have permission to use this system.
    Please see the terms and conditions for reasons as to why.


## Attributes available to us from the Active Directory system

```python

```    


