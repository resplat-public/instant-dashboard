**Managed Cloud – Specialty service.**


We want Special Capability Resources operated by RCS made available to be accessed by Researchers at the University to solve their compute problems. 

Special Capability Resources are computing resource that have additional hardware for non-general classes of methods/algorithms to be computed. For example: GPGPU nodes, Big memory nodes. Without this resource, these computation problems will either take a (very) long time on general computing, or not run at all because it is a pre-requisite. This hardware is not purchased in large quantities – they are often expensive, there is a niche demand, and technology cycles quickly (i.e. new models deprecate old ones). 

The objective for the Managed Cloud – Specialty service is to be effective at providing access to Specialty resources to Researchers that have the corresponding niche compute. Specifically:
1. Specialty resources are well utilised in producing research value:
 * “Production runs” of computations is good value. 
 * “Test runs” and debugging is necessary.
 * Researchers learning and evaluating the resource is also valuable.
 * Idle time is not valuable.
2. Low wait time for a researcher to get access to a specialty resource
 * From the request through to access

These two goals can conflict – if capacity is full and everyone is productive, then wait time for next resource available is high. We can aggressively reclaim resources frequently to reduce wait time, but then small access timeframes will mean people cannot get to a productive state.

**Proposal**:

Add a feature to Researcher Workspace. They can see a list of Specialty resources information about them. Resources are requested, and a wait-list is used to handle incoming demand. Resources are provision for a limited timeframe before the system automatically reclaims it.





**Processes:**

*Request:*

Researchers fill out a form to request the resource including contact details and use case and reason. Once submitted, the system notifies the Specialty Admin to review the request. The Admin can clarify the use case and reason outside the system through contacting the user. This may lead to the user amending the details of the request [and/or the admin doing the same]. The Admin will finally either approve or reject the request. Approved requests enter the wait-list for that resource. Rejected requests are simply discarded from review. Users are notified on the outcome.

*Provisioning:*

Wait lists operate on a first-in-first-out basis. An automated system manages the provisioning of pending requests from the wait list. It will routinely check the availability of the specialty resource and provision as managed pending requests as it can until the next check. As each machine is successfully launched, it will notify the requestor and admin that the machine is now available. [Failed launches should notify admin.]

*Reclaiming resources:*

Each resource is provisioned to a requestor for a period of time (e.g. 1 month) before they are automatically reclaimed. 
First, a user will be warned prior to expiration (e.g. 1 week) that their resources will be reclaimed. [The user may contact the administrator to apply for an extension. The administrator can extend the expiration date through the system]. When expiration of a resource is detected, the system will shelf (terminate but retain underlying volume) the machine. The user will be notified this action has been taken.

!!! See questions.

**Auxiliary Processes:**

User actions:
* Users may check their active requests and status of. User may cancel the request (e.g. waiting for approval, waiting in wait list)
* Users may opt to release the resource before their allocated time completes. Users will collect their work from the machine before shelving the virtual machine.

Admin actions: 
* They may review the current pending request list to approve/reject/follow-up to manage the request list. List should display age of request.
* They may review the status of current provisions for operational needs such as user follow-up. 
  * Stuck/errored provisions need to be fixed
  * Certain individuals need more follow-up, support or have extra requirements.
* They can also terminate a machine (because they have evidence that is should be released) so that resources are released.
* Admins can reorder items
  * For example, requester might tell admin that they will be away for 2 weeks whilst on the wait queue so they won't be able to use it even if they got it
* See reports/exports stats by specialty resource on:
  * Request rates, approval turnarounds, wait times with stage breakdown, cancels (drop out)
  * Error rates
  * Capacity – utilisation vs free




Questions:
* What are the request fields?
  * **ANS: Start with contact info, Use-case and reason. It will eventually need ARO, and other bits of info that Bernard will work through with us**
* What are the Special resources (include operating system)?
  * **ANS: Start with 'GPGPU - Linux' and 'GPGPU - Windows'. Once we have that working we can consider the other ones**
* Volume management: is this in addition to the Researcher Desktops?
  * If Yes: How do we manage these? Are they temporal? They need to be able to re-enter wait-list to resume their work – so is there a choice? What if they need to do something new/different (e.g. switch to windows GPGPU or go Big mem) but retain their previous work? 
    * Recommendation: They get 1 additional that is deleted if they switch product. In practice they need to copy off data before losing the machine so they need to treat it as ephemeral storage.
  * If No: How do we avoid disrupting user’s work if an automated system needs to turn off their current machine to switch them over to the special resource when they pop off the wait-list? 
    * Recommendation: Don’t re-use existing volume
  * ANS: Go with one (1) additional volume. HOWEVER, to handle exceptions the admin is able to launch 
stuff on other people's behalf. This is because the idea is that when a Specialty resources is managed by this system we remove the IaaS 
version of it. So if there is an exception we will manage it manually but through this system. 
I think this means the data model notes who the machine/Volume is for (just like the excel sheet now). This exception handling is not a priority.
* Alternatives: Why not plug these resources in to HPC? This is effectively a batch queue. 
  * (Won’t handle Windows)

