**DRAFT**

Two parts of this document:
1.  Service Description that is aligned with the Research Platform Service Catalogue
2.  More stuff that is a part of Service Description that should either be added to the catalogue or elsewhere (under DRAFT notes)

Reference: [Research Platform Service Catalogue](https://unimelbcloud.sharepoint.com/teams/ResPlatAllShare/Shared%20Documents/Forms/AllItems.aspx?id=%2Fteams%2FResPlatAllShare%2FShared%20Documents%2FPolicies%20Processes%20and%20Procedures%2FService%20Management%2FRPS%20Service%20Catalogue%202019%20v101%2Epdf&parent=%2Fteams%2FResPlatAllShare%2FShared%20Documents%2FPolicies%20Processes%20and%20Procedures%2FService%20Management)


# Product Description

Product Name: Researcher Desktop
Description:

A Personal Computer on the cloud that you can use as an extra compute resource 
for your research. You may find this useful to free your laptop/workstation or 
to have a computer running over night to do processing. The Researcher Desktop 
runs on the Cloud and uses the same University of Melbourne Windows or 
Linux (Ubuntu) desktop environment that you would be familiar with. This is a 
faster way of getting an additional computer than buying and setting one up 
yourself.

Specifications:

 * Cloud Virtual machine - 12 Core, 48GB RAM, 200 GB harddisk
 * OneDrive and UoM network share access
 * Windows 10 or Linux (Ubuntu)
 * Remote desktop access

You may use this for the duration of your research. See Eligibility for details.

## Eligibility

This service is for University of Melbourne and [Affiliate(link to list)]() 
Researchers, including:
   * Student - Masters/PhD by Research
   * Technical support staff

Your University of Melbourne login will be used to access this service.

Your access is tied to an Accountable Resource Owner (ARO) who is generally
your line manager or supervisor. Please see [ARO](link to aro) for details.

Students by Course work are not eligible for this service.

If you are currently not eligible but you are working on University of Melbourne
Research or Affiliate research, you may apply for access [here](link tbd)


## Availability and service support

(same as current in [Research Platform Service Catalogue](https://unimelbcloud.sharepoint.com/teams/ResPlatAllShare/Shared%20Documents/Forms/AllItems.aspx?id=%2Fteams%2FResPlatAllShare%2FShared%20Documents%2FPolicies%20Processes%20and%20Procedures%2FService%20Management%2FRPS%20Service%20Catalogue%202019%20v101%2Epdf&parent=%2Fteams%2FResPlatAllShare%2FShared%20Documents%2FPolicies%20Processes%20and%20Procedures%2FService%20Management)
)

But that should also have something about:
 * Scheduled maintance process (e.g. we notify by email 2 weeks in advance)
 * Unscheduled outage comms (notice by email asap, updates and closure)
 * Help desk incident turn around / or link to RPS SLA
 * Service level reporting (if we ever make that public)


## To request this service

To go [url tbd](url tbd) and log in with your University of Melbourne credientials


.

.

.



---
`Intentional blank space separator is intentional`
---

# DRAFT notes for review

The following are service description details that is not a part of the standard
RPS catalogue fields. They belong somewhere eventually.


## Access

You will need:
 * University of Melbourne login
 * Web browser
 * Remote desktop application (see doc link)

To go [url tbd](url tbd) to get started



## Acceptable use policy

(TBD)

## ARO

Accountable Resource Owner
 * What is
 * How utilisation of this service is attributed

## Applying for exception access

 * Find ARO
 * Get ARO to approve
 * (this process & function TBD)



## Exceptions & FAQ

 * Compromised (hacked) Desktop - quarantine process & recovery
 * Desktop disaster recovery - what happens if you have a dead machine
 * I need more than one Desktop - process for applying for IaaS
 * I need more CPU/RAM - process for applying for IaaS
 * I have sensitive data - (TBD) what is senitive level vs security level suitability, what is process
 * 
 








