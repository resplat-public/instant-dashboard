# Terraform

Terraform is a framework that allows one to declare what cloud infrastructure
needs to be assembled to host your applications. In essence, it abstracts away 
the cloud infrastructure as it supports many different cloud providers 
(it has a vast list of [providers](https://www.terraform.io/docs/providers/index.html), 
which cover all of the platforms we’ll ever need). It seems to aim to be rather 
like SQL, in that you define what you want from a given provider, and it works 
out how to provide it. It supports cross cloud infrastructure composition.
 
TerraForm is not designed to install and manage software on the machines that 
it may spin up. It is hands that task over to tools such as chef/puppet etc.
 
Terraform has three parts:

* CLI: The command line interface used to control Terraform (can be seen as: 
  free single user system)
* Cloud: A hosted service to allow teams to use Terraform together (charges for 
  bigger projects/teams)
* Enterprise: An on-premise version of Terraform Cloud
 
There is different pricing for the versions and number of users that one might 
have.
 
TerraForm cloud has:

* A policy framework named Sentinel
* An API, so it can be driven by a program, by the looks of it.
* An integration with ServiceNow
* Notifications that support human intervention.


## Some useful links:

* [Terraform home page](https://www.terraform.io/)
* [Terraform tips](https://blog.gruntwork.io/terraform-tips-tricks-loops-if-statements-and-gotchas-f739bbae55f9)
* [Test resplat Cloud workspace](https://app.terraform.io/app/resplat/workspaces)