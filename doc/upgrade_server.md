## To upgrade the code on the test and production servers:

| action | command |
|--------|---------|
| ssh into the instance | `ssh -i ~/.ssh/managed_cloud ubuntu@<server ip>` |
| Stop apache running | `sudo apachectl stop` |
| Move to the git repo home directory: | `cd ~/instant-dashboard/`|
| git fetch for changes to local_settings_template | `git fetch` |
| Check local_settings.py based on template | `git diff HEAD origin/master instant_dashboard/researcher_workspace/local_settings_template.py` |
| Update the project code with a git pull/branch change | `git pull` |
| Update the features with a submodules update | `git submodule update --remote` |
| Active the virtual environment in which the app is run | `source venv-instant-dashboard/bin/activate` |
| Update pip | `python -m pip install --upgrade pip` |
| Install any new requirements and update existing requirements | `pip install --upgrade -r requirements.txt` |
| Move to the directory with manage.py in it: | `cd ~/instant-dashboard/instant_dashboard/`|
| Move the static files into the static files directory:| `./manage.py collectstatic` |
| Make any required migrations | `./manage.py migrate` |
| restart the rq_worker and rq_scheduler | `sudo systemctl restart rqworker`
| | `sudo systemctl restart rqscheduler.service` |
| Start apache running again | `sudo apachectl start` |
 
Open https://<server> in your browser, and check that everything is working. 

If not, start debugging!

(Note that the Apache logs can be found at: `/var/log/apache2/`)
Also, that in the /etc/apache2/sites-enabled/dashboard.conf file, the LogLevel setting can be uncommented and set to
debug (`LogLevel debug`) to get more information from apache.
