# User Model

## Types of user
 
We only envisage four types of actor when it comes to user types:

1. **General public:** able to look at the landing page, terms and conditions, 
  and login dialogue, but not perform any other action;
1. **Researcher:** able to look at the landing page and login, and once logged 
  in, launch and access VM’s;
1. **Report viewer:** able to look at any reports the application may generate;
1. **Administrator:** Same rights as a researcher, but also with the extra 
  administrator rights below.

At a future date, individual researchers may be able to access and use
specialised resources, such as instances with GPGPU capabilities. But at the
moment we aren't choosing to support this.

Also, students who are doing course work **should not be** classified as
researchers. This may require extra group information from active directory.
A downside is that we will only be able to check who these users are once they
have tried to log in to the system.

Further, any expected reports will be in line with what we do now. Hence we 
require information from the LDAP/Active Directory system that we currently 
don't get...
 
## Administrator rights:
 
The only administrator actions foreseen are:

1. To view and use the Django admin dashboard to manage database records;
1. To view any reports the application may generate;
1. To Message users, either by a messaging framework or by means of email.

Administrators will **not be able to** launch VM’s on behalf of researchers.
 
For technical details on ways of implementing the Django user model see 
https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html.
If the above requirements change, we may have to alter the way in which we 
implement the user model.

 