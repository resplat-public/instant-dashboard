from researcher_desktop.constants import NOTIFY_VM_PATH_PLACEHOLDER
from researcher_workspace.settings import NOTIFY_URL
from vm_manager.constants import SCRIPT_OKAY, CLOUD_INIT_FINISHED, CLOUD_INIT_STARTED,\
    HOSTNAME_PLACEHOLDER, USERNAME_PLACEHOLDER, DOMAIN_PLACEHOLDER

user_data_windows = f"""#ps1
$ip = (Get-NetIPAddress -InterfaceAlias "Ethernet*" -AddressFamily "IPv4").IPAddress
$hostname = "{HOSTNAME_PLACEHOLDER}"
curl -o - "{NOTIFY_URL}{NOTIFY_VM_PATH_PLACEHOLDER}?ip=$ip&hn=$hostname&state={SCRIPT_OKAY}&os=windows&msg={CLOUD_INIT_STARTED}"
Start-Process -FilePath 'C:\SLAFolder\Scripts\DomainJoin.exe'
Start-Sleep -s 25
Add-LocalGroupMember -Group "Administrators" -Member "{DOMAIN_PLACEHOLDER}\{USERNAME_PLACEHOLDER}"
curl -o - "{NOTIFY_URL}{NOTIFY_VM_PATH_PLACEHOLDER}?ip=$ip&hn=$hostname&state={SCRIPT_OKAY}&os=windows&msg={CLOUD_INIT_FINISHED}"
Restart-Computer -Force
"""
