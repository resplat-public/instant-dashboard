import os
from pathlib import Path

import researcher_workspace

parent_dir = os.path.dirname(researcher_workspace.__file__)
script_path = Path(f"{parent_dir}/static/resplat.css")
CSS_VERSION = int(os.path.getmtime(script_path))

USAGE = {'CPU Hours': 0, 'Disk GB-Hours': 0, 'RAM MB-Hours': 0, 'Servers Activity': 0}

ARO_ELIGIBILITY_URL = ''
