# Copy this file to make your own local_settings.py

### DJANGO Settings ### 

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Django Security Settings
# SECURITY RECOMMENDATION: Set the following to False for dev env!
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ""  # ¡Change!

# For test/prod server environment (comment out the developer version)
# ALLOWED_HOSTS = ['<Fully Qualified Domain Name>']

# For development environment (comment out the server version)
# ALLOWED_HOSTS = ['localhost', '<tunnel ip>']

# For server setup you should set this
# STATIC_ROOT = <path to static root>

# List of people who are emailed when web app encounters an error
ADMINS = ""  # ¡Change!

### OpenStack Settings ###

# OpenStack credentials to use for this application
# See:
# > openstack application credential create <name>
# > openstack application credential list
OS_APPLICATION_CREDENTIAL_ID = ""  # ¡Change!
OS_APPLICATION_CREDENTIAL_SECRET = ""  # ¡Change!

# OpenStack Key you have access
KEYNAME = ""  # ¡Change!

### Researcher Desktop Settings

# Banner label on site only visible by users with is_superuser=True
ENVIRONMENT_NAME = ""  # ¡Change!
ENVIRONMENT_COLOR = ""  # ¡Change!

# End-point for launching desktops to tell the server success/error
# Base url only: e.g. http://<server>:<port>
NOTIFY_URL = ""  # ¡Change!

# Base URL for this site, used by Invite-link functionality as the prefix to links
# Base url only: e.g. http://<server>:<port>
SITE_URL = ""  # ¡Change!

# Volume Names on the OpenStack tenancy for different Desktops
LINUX_IMAGE_NAME = ""  # ¡Change!
WINDOWS_IMAGE_NAME = ""  # ¡Change!
TCAT_IMAGE_NAME = ""  # ¡Change!

# PostgreSQL backend settings
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': '<db_name>',
#         'USER': '<username>',
#         'PASSWORD': '<password>',
#         'HOST': '<host>',
#         'PORT': '5432',
#     }
# }

# User Search API Secrets
# Get the values from `pass staff-api-details`
RCS_SI_PASSWORD = ""  # ¡Change!
RCS_SI_STAFF_API_KEY = ""  # ¡Change!
RCS_SI_STUDENT_API_KEY = ""  # ¡Change!

# proxy url to set the local proxy for the user_search (this is not needed unless running on a server on the
# old network)
PROXY_URL = ""  # ¡Change!

# Set the allocation id depending upon the prod/test cloud. Purpose: Reporting
# Default value for Production Cloud is "7e479322bf754b2ca4945e18f28317ed"
ALLOCATION_ID = "" # ¡Change!

GENERAL_WARNING_MESSAGE = "A message you want all logged in users to see, for instance " \
                          "a warning about an upcoming deployment outage"

# Only set if using test allocation
# OS_NETWORK = ''
# OS_AVAILABILITY_ZONE = ''
