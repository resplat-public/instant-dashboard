"""

This file holds our customization of the LDAPBackend we are using to
authenticate users of the application.

# Login Requirements

* That the university active directory server be used to login users
* Hence that non University of Melbourne people are not allowed to login
* That people who fail to login are shown an appropriate error message
* That the login system can be extended by easily adding more rules in future

## Error messages:

If either username or password is wrong:

    Please enter a correct username and password. Note that both fields may
    be case-sensitive.

This is the default django error message. We simply keep it as is.

In addition, we are opting to show a message to the user if their login
failed that allows them to change/update their university password. We do this
by exposing the message in the login page template if there any errors in
the login form, regardless of what those errors are.

We achieve the extension of the login system by allowing 'voters' to vet users
that have logged in. To to this there is an abstract base class, LoginVoter.

The ResplatLDAPBackend will instantiate concrete implementations of this class
(specified as the list POST_LOGIN_VOTERS in setting.py. Then if a user has
logged in via the LDAP system, each instance is polled to see if it will allow
the login to proceed. If any reject the login, then the login attempt is also
rejected.

"""
import importlib
import logging
from abc import ABC, abstractmethod
from datetime import datetime, timedelta

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import User, Group
from django.core.cache import cache
from django.core.exceptions import ValidationError, PermissionDenied
from django_auth_ldap.backend import LDAPBackend

logger = logging.getLogger(__name__)


def overrides(interface_class):
    def overrider(method):
        assert (method.__name__ in dir(interface_class))
        return method

    return overrider


class LoginVoter(ABC):
    """
    Concrete subclass instances of this abstract base class are polled after a
    successful login by the ResplatLDAPBackend. If any of the calls to the
    can_user_pass methods return False then the login attempt is rejected.
    To set the subclass instances called, adjust the POST_LOGIN_VOTERS list
    in settings.py.
    """

    @abstractmethod
    def can_user_pass(self, user: User) -> bool:
        pass


class IsStaffMemberOrStudent(LoginVoter):
    """
    Allows all staff members and students to login...
    """

    @overrides(LoginVoter)
    def can_user_pass(self, user):
        if hasattr(user, 'ldap_user'):
            return any(person in user.ldap_user.attrs['auedupersontype'] for person in ['staff', 'student'])
        return False


class RecordLdapUserAttributes(LoginVoter):
    """ This class simply logs the ldap attributes that are returned on login.
    These are the typical attributes seen so far:

    # following is for a staff member login
    staff_attributes = {
        'department': ['Infrastructure Services'],
        'auedupersonsalutation': ['MR'],
        'auedupersonemailaddress': ['m.paulo@unimelb.edu.au'],
        'auedupersontype': ['staff'],
        'givenname': ['Martin'],
        'displayname': ['Martin Paulo'],
        'uid': ['mpaulo'],
        'departmentnumber': ['977H', '9770'],
        'ou': ['Infrastructure Services'],
        'objectclass': ['UoMPerson', 'shadowAccount', 'posixGroup',
                        'radiusprofile', 'posixAccount', 'unimelbPerson',
                        'eduMember', 'orclOpenLdapObject', 'oblixorgperson',
                        'person', 'organizationalPerson', 'inetOrgPerson',
                        'oblixPersonPwdPolicy', 'auEduPerson',
                        'sunFMSAML2NameIdentifier', 'top', 'eduPerson'],
        'cn': ['Martin Paulo'],
        'sn': ['Paulo'],
        'uidnumber': ['158455'],
        'mail': ['martin.paulo@unimelb.edu.au'],
        'gidnumber': ['10000']
    }
    # following is for a student login
    student_attributes = {
        'department': ['Science', 'Mathematics and Statistics',
                       'Computing and Information Systems'],
        'auedupersonsalutation': ['MR'],
        'auedupersonemailaddress': ['kaushik.ramesh@student.unimelb.edu.au'],
        'auedupersontype': ['student'],
        'givenname': ['Kaushik'],
        'auedupersonsubtype': ['postgrad'],
        'displayname': ['Kaushik Ramesh'],
        'uid': ['rameshk'],
        'departmentnumber': ['620M', '603M', '418M'],
        'ou': ['Science', 'Mathematics and Statistics',
               'Computing and Information Systems'],
        'objectclass': ['UoMPerson', 'shadowAccount', 'posixGroup',
                        'radiusprofile', 'posixAccount', 'unimelbPerson',
                        'eduMember', 'oblixorgperson',
                        'person', 'organizationalPerson', 'inetOrgPerson',
                        'oblixPersonPwdPolicy', 'auEduPerson',
                        'sunFMSAML2NameIdentifier', 'top', 'eduPerson'],
        'cn': ['Kaushik Ramesh'],
        'sn': ['Ramesh'],
        'uidnumber': ['677763'],
        'mail': ['kaushik.ramesh@student.unimelb.edu.au'],
        'gidnumber': ['10000']
    }

    """

    @overrides(LoginVoter)
    def can_user_pass(self, user):
        if hasattr(user, 'ldap_user'):
            logger.info('Ldap attributes: %s', user.ldap_user.attrs)
        # we don't want to affect the other LoginVoters votes.
        return True


class ResplatLDAPBackend(LDAPBackend):
    """
    This class is the customization of the LDAPBackend. In addition to the
    voting system described above, it also allows only 4 login attempts every
    5 minutes.
    """

    def __init__(self):
        LDAPBackend.__init__(self)
        self.post_login_voters = [self.str_to_instance(voter) for voter in
                                  settings.POST_LOGIN_VOTERS if
                                  voter is not None]

    @staticmethod
    def str_to_instance(name):
        """Returns a class instance from a string reference.
        If there was any problem in finding and creating the class,
        returns None
        """
        module_name, class_name = name.rsplit(".", 1)
        instance_ = None
        try:
            module_ = importlib.import_module(module_name)
            try:
                instance_ = getattr(module_, class_name)()
            except AttributeError:
                logger.error(f'Class {class_name} does not exist')
        except ImportError:
            logger.error(f'Module {module_name} does not exist')
        return instance_ or None

    default_settings = {
        "LOGIN_COUNTER_KEY": "CUSTOM_LDAP_LOGIN_ATTEMPT_COUNT",
        "LOGIN_ATTEMPT_LIMIT": settings.LOGIN_ATTEMPT_LIMIT,
        "RESET_TIME": settings.LOGIN_RESET_TIME
    }

    @overrides(LDAPBackend)
    def authenticate_ldap_user(self, ldap_user, password):
        if self.exceeded_login_attempt_limit():
            exceeded_ = f'The limit of {self.settings.LOGIN_ATTEMPT_LIMIT} ' \
                        f'login attempts within {self.settings.RESET_TIME // 60} ' \
                        f'minutes has been exceeded. Please try again in ' \
                        f'{self.settings.RESET_TIME // 60} minutes...'
            raise ValidationError(exceeded_)
        user = ldap_user.authenticate(password)
        if user:
            has_all_votes = True
            for voter in self.post_login_voters:
                if voter:
                    has_all_votes = has_all_votes and voter.can_user_pass(user)
            if has_all_votes:
                return user
        self.increment_login_attempt_count()
        return None

    @property
    def login_attempt_count(self):
        return cache.get_or_set(
            self.settings.LOGIN_COUNTER_KEY, 0, self.settings.RESET_TIME
        )

    def increment_login_attempt_count(self):
        try:
            cache.incr(self.settings.LOGIN_COUNTER_KEY)
        except ValueError:
            cache.set(self.settings.LOGIN_COUNTER_KEY, 1,
                      self.settings.RESET_TIME)

    def exceeded_login_attempt_limit(self):
        return self.login_attempt_count >= self.settings.LOGIN_ATTEMPT_LIMIT
