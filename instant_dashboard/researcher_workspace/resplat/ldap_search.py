import typing

from ldap3 import Server, Connection, ALL, AUTO_BIND_DEFAULT, AUTO_BIND_NO_TLS
from ldap3.abstract.entry import Entry

from django.conf import settings
from django.contrib.auth import get_user_model

UserModel = get_user_model()


class LDAPDoesNotExist(Exception):
    def __init__(self, *args: str) -> None:
        args = ('No entry found for search.',) + args
        super().__init__(args)


class LDAPMultipleObjectsReturned(Exception):
    def __init__(self, *args: str) -> None:
        args = ('Multiple entries returned for search.',) + args
        super().__init__(args)


class LDAPUser(object):

    def __init__(
        self,
        uid: str,
        mail: str,
        auEduPersonSalutation: str = '',
        givenName: str = '',
        surname: str = '',
        commonName: str = '',
        auEduPersonType: str = '',
        department: typing.Iterable[str] = [],
        departmentNumber: typing.Iterable[str] = [],
    ) -> None:
        self.uid = uid
        self.mail = mail
        self.auEduPersonSalutation = auEduPersonSalutation
        self.givenName = givenName
        self.surname = surname
        self.commonName = commonName
        self.auEduPersonType = auEduPersonType
        self.department = department
        self.departmentNumber = departmentNumber

    def __repr__(self) -> str:
        return f'<LDAPUser: {self.uid} ({self.mail})>'

    def __eq__(self, other):
        try:
            return self.uid == other.uid
        except AttributeError:
            return False


class LDAP(object):
    BASE_DN = 'ou=people,o=unimelb'
    # User attributes, see ServiceNow KB0011777 for details
    fields = ('uid', 'mail', 'auEduPersonSalutation', 'givenName', 'surname',
              'commonName', 'auEduPersonType', 'departmentNumber',
              'department')

    def __init__(
        self,
        uid: typing.Optional[str] = None,
        password: typing.Optional[str] = None
    ):
        host = getattr(settings, 'LDAP_HOST',
                       'ldaps://centaur.unimelb.edu.au:636')
        timeout = getattr(settings, 'LDAP_TIMEOUT', 10)
        ciphers = getattr(
            settings, 'LDAP_CIPHERS',
            'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:'
            'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:'
            'ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:'
            'ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:'
            'ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256')
        self._server = Server(host, get_info=ALL, connect_timeout=timeout)
        if self._server.tls is not None:
            self._server.tls.ciphers = ciphers
        if uid and password:
            self._conn = Connection(self._server, f'uid={uid},{self.BASE_DN}',
                                    password=password,
                                    auto_bind=AUTO_BIND_NO_TLS)
        else:
            self._conn = Connection(self._server, self.BASE_DN)

    def _create_ldap_AND_filter(self, **kwargs: str) -> str:
        return ''.join(f'({k}={v})' for k, v in kwargs.items()).join(('(&', ')'))

    def _entry_to_ldapuser(self, entry: Entry) -> LDAPUser:
        user_dict = {a: getattr(entry, a).value for a in self.fields}
        user_dict['departmentNumber'] = [
            n for n in entry.departmentNumber.values if n and 'H' not in n]
        user_dict['department'] = entry.department.values
        return LDAPUser(**user_dict)

    @property
    def entries(self) -> typing.List[LDAPUser]:
        return [self._entry_to_ldapuser(e) for e in self._conn.entries]

    def search(self, filter: str) -> None:
        if self._conn.auto_bind == AUTO_BIND_DEFAULT:
            self._conn.open()
        self._conn.search(self.BASE_DN, filter, attributes=self.fields)

    def get(self, **kwargs: str) -> LDAPUser:
        self.search(self._create_ldap_AND_filter(**kwargs))
        if not self._conn.entries:
            raise LDAPDoesNotExist(self._conn.request)
        elif len(self._conn.entries) > 1:
            raise LDAPMultipleObjectsReturned(self._conn.request)
        return self._entry_to_ldapuser(self._conn.entries[0])

    def list(self, **kwargs: str) -> typing.List[LDAPUser]:
        self.search(self._create_ldap_AND_filter(**kwargs))
        return self.entries
