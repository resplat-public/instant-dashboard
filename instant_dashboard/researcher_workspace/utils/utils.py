import requests
from researcher_workspace.constants import ARO_ELIGIBILITY_URL


# This function is using the 'requests' module to do a post request to 'RCS PowerAutomate'
# More details about the module 'requests': https://docs.python-requests.org/en/latest/
def check_aro_eligibility(email):
    url = ARO_ELIGIBILITY_URL
    data = {'email': email}
    try:
        r = requests.post(url, json=data)
        return r
    except Exception as e:
        raise e
