"""
WSGI config for instant_dashboard project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

from researcher_workspace.settings import RCS_SI_PASSWORD, RCS_SI_STAFF_API_KEY, RCS_SI_STUDENT_API_KEY, PROXY_URL

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'researcher_workspace.settings')

application = get_wsgi_application()

os.environ["RCS_SI_PASSWORD"] = RCS_SI_PASSWORD
os.environ["RCS_SI_STAFF_API_KEY"] = RCS_SI_STAFF_API_KEY
os.environ["RCS_SI_STUDENT_API_KEY"] = RCS_SI_STUDENT_API_KEY

if PROXY_URL:
    os.environ["http_proxy"] = PROXY_URL
    os.environ["https_proxy"] = PROXY_URL
    os.environ["HTTP_PROXY"] = PROXY_URL
    os.environ["HTTPS_PROXY"] = PROXY_URL
